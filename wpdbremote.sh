#!/bin/bash

set -e

## version
VERSION="0.0.1"

## Based on which does the user provides in the arguments, determines the way to fetch the database credentials
credentials() {
    if [ ! -z $1 ]; then
        user=$(cat ~/.wpdbremote/$1 | sed -n /user:/p | cut -f2 -d:)
        server=$(cat ~/.wpdbremote/$1 | sed -n /server:/p | cut -f2 -d:)
        path=$(cat ~/.wpdbremote/$1 | sed -n /path:/p | cut -f2 -d:)
    else
        ask
    fi
}

## Generates a temporary local file before be sent to server
generates_file() {
    file_full_path=$1

    touch $file_full_path
    echo "#!/bin/bash" >> $file_full_path
    echo "" >> $file_full_path
    echo "if ! cat wp-config.php | grep -i '\$table_prefix' | grep \' > /dev/null; then" >> $file_full_path
    echo "  echo Sorry! I could not fetch the database prefix" >> $file_full_path
    echo "  exit" >> $file_full_path
    echo "fi" >> $file_full_path
    echo "" >> $file_full_path
    echo "dbname=\$(cat wp-config.php | grep -i db | grep -i DB_NAME | cut -f4 -d\')" >> $file_full_path
    echo "dbuser=\$(cat wp-config.php | grep -i db | grep -i DB_USER | cut -f4 -d\')" >> $file_full_path
    echo "dbhost=\$(cat wp-config.php | grep -i db | grep -i DB_HOST | cut -f4 -d\')" >> $file_full_path
    echo "dbpass=\$(cat wp-config.php | grep -i db | grep -i DB_PASSWORD | cut -f4 -d\')" >> $file_full_path
    echo "db_prefix=\$(cat wp-config.php | grep -i 'table_prefix' | cut -f2 -d\')" >> $file_full_path
    echo "for i in \$(mysql -u\$dbuser -p\$dbpass -h\$dbhost \$dbname -e); do "
    # echo "mysqldump -u\$dbuser -p\$dbpass -h\$dbhost \$dbname > db.$data.sql" >> $file_full_path
}

## Make initial questions
ask() {
    read -p "Provides the server " server
    read -p "Provides the user " user
    read -p "Provides the server path " path
}

execute_remote() {
    ssh $user@$server << EOF
    cd $path
    chmod +x $filename
    sh $filename
    rm $filename
EOF
}

credentials $1

data=$(date +%Y%m%d-%Hh%Mm%Ss)
filename=tmpscript.$data.sh
filepath=/tmp/
file_full_path=$filepath$filename

generates_file $file_full_path

scp $file_full_path $user@$server:/$path

execute_remote

echo The remote script: $filename

## detect if being sourced and
## export if so else execute
## main function with args
# if [[ ${BASH_SOURCE[0]} != $0 ]]; then
#   export -f wpdbremote
# else
#   wpdbremote "${@}"
#   exit $?
# fi