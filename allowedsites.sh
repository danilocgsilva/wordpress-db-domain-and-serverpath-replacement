#!/bin/bash

set -e

if [ "$#" -ne 1 ]; then
    echo Supply the first site
    exit
fi

if !([ $1 = profis ] || [ $1 = www ] || [ $1 = ambiental ] || [ $1 = farma ] || [ $1 = nutricai ] ); then
    echo Supplied a not allowed site
    exit
fi
