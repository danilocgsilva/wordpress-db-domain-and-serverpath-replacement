# My project's README

Make domain and server path replacements in a database script file.

Made for make ajustments in a mysql script from a WordPress that changes the computer where it runs. If you are moving your WordPress production site to a development machine, or vice-versa, you must make some simple string replacements to let it work seamlessly.

## BUT WARNING!!!

It works well in simple WordPress sites. Depending the way the site was mantained, the plugins and themes installed, the replacements provided by this script may not be enough. So caution!