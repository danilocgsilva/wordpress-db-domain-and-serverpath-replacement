
BIN ?= wpdbremote
PREFIX ?= /usr/local

install:
	cp wpdbremote.sh $(PREFIX)/bin/$(BIN)
	chmod +x $(PREFIX)/bin/$(BIN)

uninstall:
	rm -f $(PREFIX)/bin/$(BIN)
	